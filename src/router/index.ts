import Vue from 'vue'
import VueRouter, { RouteConfig } from 'vue-router'
import StoreView from '../views/StoreView.vue'
import StoreOrdersView from '../views/StoreOrdersView.vue'
import NetworkStatusView from '../views/NetworkStatusView.vue'
import AboutView from '../views/AboutView.vue'

Vue.use(VueRouter)

const routes: Array<RouteConfig> = [
  { path: '/', name: 'store-page', component: StoreView },
  { path: '/store-orders', name: 'store-orders-page', component: StoreOrdersView },
  { path: '/network-status', name: 'network-status-page', component: NetworkStatusView },
  { path: '/about', name: 'about-page', component: AboutView }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
  scrollBehavior (_to, _from, savedPosition) {
    if (savedPosition) {
      return savedPosition
    }
    return { x: 0, y: 0 }
  }
})

export default router
