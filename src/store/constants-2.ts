
const WEB_PAGE_INFORMATION_LIST = [{
  uid: 'store-page',
  name: 'Store',
  icon: 'storefront', // not_listed_location
  routePath: '/',
  optionList: []
}, {
  uid: 'store-requests-page',
  name: 'Store Orders',
  icon: 'receipt_long', // not_listed_location
  routePath: '/store-orders',
  optionList: []
}, {
  uid: 'network-status-page',
  name: 'Network Status',
  icon: 'stars', // not_listed_location
  routePath: '/network-status',
  optionList: []
}, {
  uid: 'about-page',
  name: 'About',
  icon: 'info_outline', // not_listed_location
  routePath: '/about',
  optionList: []
}]

const PRODUCT_QUALITY_TYPE_LIST = [
  { productQualityId: 'new', productQualityName: 'New' },
  { productQualityId: 'used-and-works', productQualityName: 'Used And Works' },
  { productQualityId: 'used-and-doesnt-work', productQualityName: 'Used And Doesn\'t Work' },
  { productQualityId: 'damaged-but-works', productQualityName: 'Damaged But Works' },
  { productQualityId: 'damaged-and-doesnt-work', productQualityName: 'Damaged And Doesn\'t Work' },
  { productQualityId: 'missing-items', productQualityName: 'Missing Items' }
]

export {
  WEB_PAGE_INFORMATION_LIST,
  PRODUCT_QUALITY_TYPE_LIST
}
