
import { electronicsList } from './inventory/electronics'
import { officeSuppliesList } from './inventory/office-supplies'

const PRODUCT_RESOURCE_TYPE_LIST = [
  { productResourceId: 'arts-and-crafts', productResourceName: 'Arts & Crafts', productResourceIconId: 'palette', productResourceInventoryList: [], productResourceOrderList: [] },
  { productResourceId: 'automotive', productResourceName: 'Automotive', productResourceIconId: 'directions_car', productResourceInventoryList: [], productResourceOrderList: [] },
  { productResourceId: 'baby', productResourceName: 'Baby', productResourceIconId: 'child_care', productResourceInventoryList: [], productResourceOrderList: [] },
  { productResourceId: 'beauty-and-personal-care', productResourceName: 'Beauty & Personal Care', productResourceIconId: 'visibility', productResourceInventoryList: [], productResourceOrderList: [] },
  { productResourceId: 'books', productResourceName: 'Books', productResourceIconId: 'menu_book', productResourceInventoryList: [], productResourceOrderList: [] },
  { productResourceId: 'computers', productResourceName: 'Computers', productResourceIconId: 'computer', productResourceInventoryList: [], productResourceOrderList: [] },
  { productResourceId: 'digital-music', productResourceName: 'Digital Music', productResourceIconId: 'headset', productResourceInventoryList: [], productResourceOrderList: [] },
  { productResourceId: 'electronics', productResourceName: 'Electronics', productResourceIconId: 'speaker', productResourceInventoryList: electronicsList, productResourceOrderList: [] },
  { productResourceId: 'womens-fashion', productResourceName: 'Women\'s Fashion', productResourceIconId: 'checkroom', productResourceInventoryList: [], productResourceOrderList: [] },
  { productResourceId: 'mens-fashion', productResourceName: 'Men\'s Fashion', productResourceIconId: 'checkroom', productResourceInventoryList: [], productResourceOrderList: [] },
  { productResourceId: 'girls-fashion', productResourceName: 'Girls\' Fashion', productResourceIconId: 'checkroom', productResourceInventoryList: [], productResourceOrderList: [] },
  { productResourceId: 'boys-fashion', productResourceName: 'Boys\' Fashion', productResourceIconId: 'checkroom', productResourceInventoryList: [], productResourceOrderList: [] },
  { productResourceId: 'grocery-and-gourmet-food', productResourceName: 'Grocery & Gourmet Foods', productResourceIconId: 'restaurant', productResourceInventoryList: [], productResourceOrderList: [] },
  { productResourceId: 'health-and-household', productResourceName: 'Health & Household', productResourceIconId: 'local_hospital', productResourceInventoryList: [], productResourceOrderList: [] },
  { productResourceId: 'home-and-kitchen', productResourceName: 'Home & Kitchen', productResourceIconId: 'home', productResourceInventoryList: [], productResourceOrderList: [] },
  { productResourceId: 'industrial-and-scientific', productResourceName: 'Industrial & Scientific', productResourceIconId: 'construction', productResourceInventoryList: [], productResourceOrderList: [] },
  { productResourceId: 'luggage', productResourceName: 'Luggage', productResourceIconId: 'work', productResourceInventoryList: [], productResourceOrderList: [] },
  { productResourceId: 'movies-and-tv', productResourceName: 'Movies & TV', productResourceIconId: 'movie', productResourceInventoryList: [], productResourceOrderList: [] },
  { productResourceId: 'music-cds-and-vinyl', productResourceName: 'Music, CDs & Vinyl', productResourceIconId: 'album', productResourceInventoryList: [], productResourceOrderList: [] },
  { productResourceId: 'office-supplies', productResourceName: 'Office Supplies', productResourceIconId: 'print', productResourceInventoryList: officeSuppliesList, productResourceOrderList: [] },
  { productResourceId: 'pet-supplies', productResourceName: 'Pet Supplies', productResourceIconId: 'pets', productResourceInventoryList: [], productResourceOrderList: [] },
  { productResourceId: 'real-estate-homes-rent', productResourceName: 'Real Estate, Homes & Rent', productResourceIconId: 'apartment', productResourceInventoryList: [], productResourceOrderList: [] },
  { productResourceId: 'software', productResourceName: 'Software', productResourceIconId: 'wysiwyg', productResourceInventoryList: [], productResourceOrderList: [] },
  { productResourceId: 'sports-and-outdoors', productResourceName: 'Sports & Outdoors', productResourceIconId: 'sports_tennis', productResourceInventoryList: [], productResourceOrderList: [] },
  { productResourceId: 'tools-and-home-improvement', productResourceName: 'Tools & Home Improvement', productResourceIconId: 'handyman', productResourceInventoryList: [], productResourceOrderList: [] },
  { productResourceId: 'toys-and-games', productResourceName: 'Toys & Games', productResourceIconId: 'toys', productResourceInventoryList: [], productResourceOrderList: [] },
  { productResourceId: 'video-games', productResourceName: 'Video Games', productResourceIconId: 'sports_esports', productResourceInventoryList: [], productResourceOrderList: [] }
]

const WORK_STATUS_TYPE_LIST = [
  { workStatusName: 'Employed' },
  { workStatusName: 'Unemployed' },
  { workStatusName: 'Retired' },
  { workStatusName: 'Homeless' },
  { workStatusName: 'Homeless Veteran' }
]

const ORDER_URGENCY_TYPE_LIST = [
  { orderUrgencyName: 'Required Within 1 Day' },
  { orderUrgencyName: 'Required Within 1 Week' },
  { orderUrgencyName: 'Required Within 2 Weeks' },
  { orderUrgencyName: 'Required Within 1 Month' },
  { orderUrgencyName: 'Required Within 3 Months' },
  { orderUrgencyName: 'Required Within 6 Months' },
  { orderUrgencyName: 'Required Within 1 Year' }
]

export {
  PRODUCT_RESOURCE_TYPE_LIST,
  WORK_STATUS_TYPE_LIST,
  ORDER_URGENCY_TYPE_LIST
}
