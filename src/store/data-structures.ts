

class ProductResource {
  productId: string = ''
  productName: string = ''
  productShortDescription?: string = ''

  imageSrcList?: string[] = []

  productDescription?: string = ''

  productCategoryId?: string = ''
  numberOfItems?: number = 0
  productQualityId?: string = ''

  uploaderName?: string = ''

  constructor (productResource?: ProductResource) {
    Object.assign(this, productResource)
  }

}

export {
  ProductResource
}


