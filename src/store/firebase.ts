
// Import the functions you need from the SDKs you need
// import { initializeApp } from 'firebase/app'
// import { getAnalytics } from 'firebase/analytics'
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

import firebase from 'firebase/compat/app'
import 'firebase/compat/auth'
import 'firebase/compat/database'
import 'firebase/compat/firestore'
import 'firebase/compat/storage'
import 'firebase/compat/analytics'

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: 'AIzaSyCYJNr6Sc3XDBtEUuuWvS0iQCCjgffQT0g',
  authDomain: 'youstore369.firebaseapp.com',
  projectId: 'youstore369',
  storageBucket: 'youstore369.appspot.com',
  messagingSenderId: '492004080904',
  appId: '1:492004080904:web:c1e7982d6d8faa993ec0bd',
  measurementId: 'G-CGL2SVPWHY'
}

// Initialize Firebase
const firebaseApp = firebase.initializeApp(firebaseConfig)
const firebaseAnalytics = firebase.analytics(firebaseApp)

export {
  firebaseApp,
  firebaseAnalytics
}
