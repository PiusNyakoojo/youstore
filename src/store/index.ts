import Vue from 'vue'
import Vuex, { StoreOptions } from 'vuex'

Vue.use(Vuex)

interface RootState {
  projectName?: string
  projectShortDescription?: string
  socialMediaTable?: any
  isMobileDevice?: boolean

  // Settings
}

const TOGGLE_IS_MOBILE_DEVICE = 'TOGGLE_IS_MOBILE_DEVICE'

// Vuex Store
const store: StoreOptions<RootState> = {
  state: {
    projectName: 'YouStore',
    projectShortDescription: 'A Freestore for the Internet', // 'The YouTube for Free Products',
    socialMediaTable: {
      gitlab: {
        imageIconUrl: 'https://ecoin-static-file-host.web.app/service-for-environment-variables/environment-variables-type-1-general-variables/general-variables-type-1-project-variables/service-for-images/images-type-3-social-media/social-media-1-gitlab-icon-60x60.png',
        resourceUrl: 'https://gitlab.com/piusnyakoojo/youstore'
      },
      youtube: {
        imageIconUrl: 'https://ecoin-static-file-host.web.app/service-for-environment-variables/environment-variables-type-1-general-variables/general-variables-type-1-project-variables/service-for-images/images-type-3-social-media/social-media-2-youtube-icon-60x60.png',
        resourceUrl: 'https://youtube.com/youstore'
      },
      twitter: {
        imageIconUrl: 'https://ecoin-static-file-host.web.app/service-for-environment-variables/environment-variables-type-1-general-variables/general-variables-type-1-project-variables/service-for-images/images-type-3-social-media/social-media-3-twitter-icon-60x60.png',
        resourceUrl: 'https://twitter.com/youstore'
      },
      facebook: {
        imageIconUrl: 'https://starpng.com/public/uploads/preview/black-facebook-f-logo-png-11574792338wy681p6sr8.png',
        resourceUrl: 'https://facebook.com/youstore'
      },
      instagram: {
        imageIconUrl: 'https://automationsensation.net/wp-content/uploads/2020/09/logo-ig-instagram-new-logo-vector-download-5.png',
        resourceUrl: 'https://instagram.com/youstore'
      }
    },

    isMobileDevice: true
  },
  mutations: {
    [TOGGLE_IS_MOBILE_DEVICE] (state, isMobile: boolean) {
      state.isMobileDevice = isMobile
    }
  }
}

const vuexStore = new Vuex.Store<RootState>(store)

export {
  vuexStore,
  TOGGLE_IS_MOBILE_DEVICE
}
