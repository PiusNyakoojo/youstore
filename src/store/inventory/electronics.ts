
import { ProductResource } from '../data-structures'

const electronicsList: ProductResource[] = [{
  productId: 'electronics-1',
  productName: 'Computer Mouse',
  numberOfItems: 1,
  productQualityId: 'used-and-works',
  imageSrcList: [
    'https://pixfeeds.com/images/topic/3639/1200-3639-computer-mouse-photo1.jpg'
  ]
}]

export {
  electronicsList
}
