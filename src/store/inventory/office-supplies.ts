
import { ProductResource } from '../data-structures'

const officeSuppliesList: ProductResource[] = [{
  productId: 'mechanical-pencils-1',
  productName: 'Mechanical Pencils',
  numberOfItems: 7,
  productQualityId: 'used-and-works',
  imageSrcList: [
    'https://s7.orientaltrading.com/is/image/OrientalTrading/VIEWER_ZOOM/rainbow-mechanical-pencil-assortment-50-pc-~13640909'
  ]
}]

export {
  officeSuppliesList
}
